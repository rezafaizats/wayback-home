﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public float moveSpeed;

    private Rigidbody2D playerRB;
    private Vector2 playerDir;

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        playerDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void FixedUpdate()
    {
        movePlayer(playerDir);
    }

    public void movePlayer(Vector2 dir) {
        playerRB.MovePosition((Vector2)transform.position + (dir * Time.deltaTime * moveSpeed));
    }

}
